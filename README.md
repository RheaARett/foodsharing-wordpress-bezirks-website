# Theme: foodsharing Bezirks Website

Maintainer: Christian Walgenbach  
Contact: wp-bezirk-website@fs-bezirk.de

## Intro
This project aims to provide a theme for the individual foodsharing districts, simple installation and documentation tools.

Let's develop this project after the open-source idea.

The info page is on https://wordpress-style.fs-bezirk.de available.
The current demo page can be found at https://wp-style-demo.fs-bezirk.de/

## Deploy a docker develop and test environment

https://gitlab.com/foodsharing-dev/ag-bezirkswebsite/foodsharing-wordpress-bezirks-website/wikis/Getting-the-code