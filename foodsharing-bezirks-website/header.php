<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package foodsharing_bezirks_style
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>//favicon/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="<?php echo esc_url( get_parent_theme_file_uri( '/' )); ?>/favicon/mstile-310x310.png" />

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'foodsharing-bezirks-website' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
	<header id="masthead" class="site-header navbar sticky-top <?php echo foodsharing_bezirks_style_bg_class(); ?>" role="banner">
        <div class="container">
            <nav class="navbar navbar-expand-xl p-0" role="navigation">
            <div class="navbar-brand">
                    <?php if ( get_theme_mod( 'foodsharing_bezirks_style_logo' ) ): ?>
                        <a href="<?php echo esc_url( home_url( '/' )); ?>">
                            <img src="<?php echo esc_attr(get_theme_mod( 'foodsharing_bezirks_style_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                        </a>
                    <?php else : ?>
                        <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><div class="fs-title-brown"><span>food</span><span class="fs-title-green">sharing </span><div class="fs-title-bezirk"><?php echo get_theme_mod( 'foodsharing_bezirk_title_setting' ); ?></div></div></a>
                    <?php endif; ?>

</div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
   wp_nav_menu(array(
    'theme_location'    => 'primary',
    'container'       => 'div',
    'container_id'    => 'main-nav',
    'container_class' => 'collapse navbar-collapse justify-content-start',
    'menu_id'         => false,
    'menu_class'      => 'navbar-nav',
    'depth'           => 2,
    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
        'walker'         => new WP_Bootstrap_Navwalker(),
      ));
                ?>

            </nav>
        </div>
	</header><!-- #masthead -->
    <?php if(is_front_page() && !get_theme_mod( 'header_banner_visibility' )): ?>
        <div id="page-sub-header" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
            <div class="container">
                <h1>
                    <?php
                    if(get_theme_mod( 'header_banner_title_setting' )){
                        echo get_theme_mod( 'header_banner_title_setting' );
                    }else{
                        esc_html_e('Verwenden statt verschwenden!', 'foodsharing-bezirks-website');
                    }
                    ?>
                </h1>
                <p>
                    <?php
                    if(get_theme_mod( 'header_banner_tagline_setting' )){
                        echo get_theme_mod( 'header_banner_tagline_setting' );
                }else{
                    esc_html_e('Seit &uuml;ber 6 Jahren retten wir Tag f&uuml;r Tag Lebensmittel vor dem M&uuml;ll. Eine Tonne nach der anderen.', 'foodsharing-bezirks-website');
                    }
                    ?>
                </p>
                <a href="#content" class="page-scroller"><i class="fa fa-fw fa-angle-down"></i></a>
            </div>
        </div>
    <?php endif; ?>
	<div id="content" class="site-content">
		<div class="container">
			<div class="row flex-nowrap">
                <?php endif; ?>