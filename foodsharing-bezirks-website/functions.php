<?php
/**
 * WP Bootstrap Starter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package foodsharing_bezirks_style
 */

if ( ! function_exists( 'foodsharing_bezirks_style_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function foodsharing_bezirks_style_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WP Bootstrap Starter, use a find and replace
	 * to change 'foodsharing-bezirks-website' to the name of your theme in all the template files.
	 
	load_theme_textdomain( 'foodsharing-bezirks-website', get_template_directory() . '/languages' );*/

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'foodsharing-bezirks-website' ),
		'secondary' => esc_html__( 'Secondary', 'foodsharing-bezirks-website' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'foodsharing_bezirks_style_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

    function wp_boostrap_foodsharing_bezirks_style_add_editor_styles() {
        add_editor_style( 'custom-editor-style.css' );
    }
    add_action( 'admin_init', 'wp_boostrap_foodsharing_bezirks_style_add_editor_styles' );

}
endif;
add_action( 'after_setup_theme', 'foodsharing_bezirks_style_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function foodsharing_bezirks_style_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'foodsharing_bezirks_style_content_width', 1170 );
}
add_action( 'after_setup_theme', 'foodsharing_bezirks_style_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function foodsharing_bezirks_style_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'foodsharing-bezirks-website' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'foodsharing-bezirks-website' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 1', 'foodsharing-bezirks-website' ),
        'id'            => 'footer-1',
        'description'   => esc_html__( 'Add widgets here.', 'foodsharing-bezirks-website' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 2', 'foodsharing-bezirks-website' ),
        'id'            => 'footer-2',
        'description'   => esc_html__( 'Add widgets here.', 'foodsharing-bezirks-website' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 3', 'foodsharing-bezirks-website' ),
        'id'            => 'footer-3',
        'description'   => esc_html__( 'Add widgets here.', 'foodsharing-bezirks-website' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'foodsharing_bezirks_style_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function foodsharing_bezirks_style_scripts() {
    // load bootstrap css
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/normalize.css' );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/inc/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'foodsharing-bezirks-website', get_template_directory_uri() . '/style.css' );
	wp_enqueue_script('jquery');
    wp_enqueue_script('foodsharing-bezirks-website-bootstrapjs', get_template_directory_uri() . '/inc/assets/js/bootstrap.min.js', array(), '', true );
  
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'foodsharing_bezirks_style_scripts' );


function foodsharing_bezirks_style_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <div class="d-block mb-3">' . __( "To view this protected post, enter the password below:", "foodsharing-bezirks-website" ) . '</div>
    <div class="form-group form-inline"><label for="' . $label . '" class="mr-2">' . __( "Password:", "foodsharing-bezirks-website" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" class="form-control mr-2" /> <input type="submit" name="Submit" value="' . esc_attr__( "Submit", "foodsharing-bezirks-website" ) . '" class="btn btn-primary"/></div>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'foodsharing_bezirks_style_password_form' );



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load custom WordPress nav walker.
 */
if ( ! file_exists( get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php' ) ) {
	// file does not exist... return an error.
	return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
	// file exists... require it.
	require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
}

//Initialize the update checker.
require 'inc/theme-update-checker.php';
$example_update_checker = new ThemeUpdateChecker(
    'foodsharing-bezirks-website',
    'https://wordpress-style.fs-bezirk.de/updater/info.json'
);